# Android Boot Animations from GIFs
This utility provides a simple way to convert GIF files into
`bootanimation.zip` files that are compatible with Android. The format for
boot animation files can be found
[here](https://android.googlesource.com/platform/frameworks/base/+/master/cmds/bootanimation/FORMAT.md).


## Background
I wrote this to convert GIFs from [gifcities.org](https://gifcities.org/) into
boot animations for my phone. This utility is intended to scale graphics up
**without** blurring so [jaggies](https://en.wikipedia.org/wiki/Jaggies) are
preserved (yes, the animations are supposed to look *bad*). This is mostly for
my own use and for phone hackers that are comfortable with using a CLI.


## Requirements
This utility is a POSIX-compatible shell script that requires 
[Gifsicle](http://www.lcdf.org/gifsicle/),
[ImageMagick](https://imagemagick.org/), and
[Pngcrush](https://pmt.sourceforge.io/pngcrush/) to be installed.


## Installation
Install the prerequisite utilities using your distros package manager and copy
the script `gif2bootanim` where you will be working with files. Run
`chmod +x gif2bootanim` to make the script executable.


## Usage
To convert a GIF using the default settings (100% scale, 10fps, black
background):
```
./gif2bootanim input.gif
```

If you find the image is too small, increase the scale:
```
./gif2bootanim input.gif 200%
```

The default FPS is 10 to match the GIF format's default. For faster animations
change it to something a little higher like 20 or 30:
```
./gif2bootanim input.gif 200% 20
```

The default background color is black but can be overidden with an RGB HEX
color: **Note: the quotes around the color are required.**
```
./gif2bootanim input.gif 200% 20 "#40E0D0"
```


## Animation Installation
**This utility can only be used to generate boot animations; it cannot install
the generated animations on its own.** To install the boot animation you will
need a host computer with ADB installed and a device that will allow root ADB
sessions.

First, backup the existing animation:
```
adb pull /system/media/bootanimation.zip bootanimation.zip.backup
```

Then, push the new animation to the device:
```
adb root
adb remount
adb push bootanimation.zip /system/media
```

At this point it's advisable to reboot since partitions have been remounted as
writable:
```
adb reboot
```

If all went well you should see the animation while the phone is booting.


## Licensing
Source in this repository is licensed under the 2-clause BSD license, see
`LICENSE` for details.
